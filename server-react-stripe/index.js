const express= require('express');
const stripe = require('stripe');
const cors = require('cors');
const app = express();
app.use(cors({origin: 'http://localhost:3000'}));
app.use(express.json());
let strip = new stripe("sk_test_mg3eJ7qrjvcvJc4mQQBorztq00885ZaU2T");
app.post('/api/checkout', async (req,res) =>{
    try {
          let { id, amount } = req.body;
          const payment = await strip.paymentIntents.create({
            amount,
            currency: "USD",
            description: "teclado",
            payment_method: id,
            confirm: true,
          });
          console.log(payment);
          res.send({ message: "exitoso pago" });
        
    } catch (error) {
        console.log(error);
        res.json({message: error.raw.message });
    }
  
})

app.listen(8080, ()=>{
    console.log("server on port ", 8080);
});
